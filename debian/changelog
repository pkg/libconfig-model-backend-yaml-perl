libconfig-model-backend-yaml-perl (2.134-2+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 17:17:02 +0000

libconfig-model-backend-yaml-perl (2.134-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 16:24:34 +0100

libconfig-model-backend-yaml-perl (2.134-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 16:11:44 +0000

libconfig-model-backend-yaml-perl (2.134-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 2.134.
  * Bump versioned build dependency on libconfig-model-tester-perl.
  * Update debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 Jan 2021 20:03:20 +0100

libconfig-model-backend-yaml-perl (2.133-2co1) apertis; urgency=medium

  * debian/apertis/component: Set to development
  * debian/apertis/gitlab-ci.yml: Drop since we use an external definition

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 01 Mar 2021 08:06:55 +0000

libconfig-model-backend-yaml-perl (2.133-2) unstable; urgency=medium

  * Team upload.
  * Add Breaks+Replaces on old libconfig-model-perl which still contained
    the YAML backend.
    Thanks to Andreas Beckmann for the bug report. (Closes: #919909)
  * Don't install t/README.md.
  * Update debian/upstream/metadata.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Jan 2019 19:05:59 +0100

libconfig-model-backend-yaml-perl (2.133-1) unstable; urgency=medium

  * New upstream version 2.133
    * fix read/write of UTF-8 data
  * control: declare compliance with policy 4.3.0

 -- Dominique Dumont <dod@debian.org>  Sun, 30 Dec 2018 12:18:04 +0100

libconfig-model-backend-yaml-perl (2.132-1) unstable; urgency=low

  * Initial release. (Closes: #916868)
    Provides a Perl module that was shipped with
    libconfig-model-perl up to version 2.131.

 -- Dominique Dumont <dod@debian.org>  Wed, 19 Dec 2018 19:34:13 +0100
